<?php

	include 'basedatos.php';
	include 'vendor/twig.php';
	

	try {
  		$query = "SELECT * FROM aplicacion_recetas";
  		$stmt = $db->prepare($query);
  		$stmt->execute();
  		$result = $stmt->fetchAll();
	} catch (PDOException $e) {
  		echo $e->getMessage();
	}

	// Aplicamos los cambios en el listado de las recetas:
	echo $twig->render('listado_recetas.html', array('recetas' => $result));

?>