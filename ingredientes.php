<?php

	class Ingrediente {
		public $id;
	  	public $nombre;
	
	  	public function __construct($nombre) {
	    	$this->nombre = ucfirst($nombre);
	  	}
	
	  	public static function obtenerIdIngredientes($db, $nombre) {
	    	try {
	      		$query = "SELECT * FROM aplicacion_ingredientes WHERE nombre = :nombre COLLATE NOCASE";
	      		$stmt = $db->prepare($query);
	      		$stmt->execute(array(':nombre' => $nombre));
	      		$resultado = $stmt->fetch();
	    	} catch(PDOException $e) {
	      		echo $e->getMessage();
	    	}
	
	    	return $resultado["id"];
	  	}
	
	
	  	public function guardarIngredientes($db) {
	    	try {
	      		$insert = "INSERT INTO aplicacion_ingredientes (nombre) VALUES (:nombre)";
	      		$stmt = $db->prepare($insert);
	      		$stmt->execute(array("nombre" => $this->nombre));
	    	} catch (PDOException $e) {
	      		echo $e->getMessage();
	    	}
	
	    	return true;
	  	}
	}

?>