<?php

	include 'basedatos.php';
	include 'recetas.php';
	
	// Redirigimos al usuario si no está logeado
	session_start();
	
	if (!isset($_SESSION["id"])) {
		header('location: index.php');
	 	exit;
	}
	
	// Procedemos a borrar la receta
	$idReceta = $_GET['id'];
	if (!Receta::borrar($db, $_SESSION["id"], $idReceta)) {
		header('location: index.php');
	  	exit;
	}
	
	// Una vez eliminada, redirigimos al index
	header('location: index.php');

?>