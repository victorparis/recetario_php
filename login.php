<?php

	include 'basedatos.php';
	include 'vendor/twig.php';
	include 'usuarios.php';


	// Si llamamos al archivo por POST
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
	  	
		// Obtenemos los datos del formulario
		$user = trim($_POST['user']);
	  	$pass = trim($_POST['pass']);
	
	  	// Logeamos el usuario
	  	$usuario = new Usuario($user, $pass);
	
	  	if (!$usuario->login($db)) {
	    	$error = "El usuario o la contraseña introducidos son incorrectos.";
	    	echo $twig->render('login.html', array("error" => $error));
	    	return;
	  	}
	
	  	// Volvemos al index logeados
	  	header('location: index.php');
	  	exit;
	}
	
	// Aplicamos cambios en el template del registro de usuario
	echo $twig->render('login.html');

?>