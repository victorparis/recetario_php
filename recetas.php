<?php

	class Receta {
	  	public $id;
	  	public $nombre;
	  	public $tipo;
	  	public $elaboracion;
	  	public $tiempo;
	  	public $imagen;
	
	  	public function __construct($id, $data, $img) {
		    $this->id = $id;
		    $this->nombre = trim($data["nombre"]);
		    $this->tipo = $data["tipo"];
		    $this->elaboracion = trim($data["ela"]);
		    $this->tiempo = trim($data["tiempo"]);
		    $this->imagen = $img;
	  	}
	
	  	public static function obtenerIdReceta($db, $nombre) {
	    	try {
	      		$query = "SELECT * FROM aplicacion_recetas WHERE nombre = :nombre COLLATE NOCASE";
	      		$stmt = $db->prepare($query);
	      		$stmt->execute(array(':nombre' => $nombre));
	      		$resultado = $stmt->fetch();
	    	} catch(PDOException $e) {
	      		echo $e->getMessage();
	    	}
	
	    	return $resultado["id"];
	  	}
	
	  	public static function borrarIngrediente($db, $id) {
	    	try {
	      		$delete = "DELETE FROM aplicacion_union WHERE receta_id = :id";
	      		$stmt = $db->prepare($delete);
	      		$stmt->execute(array("id" => $id));
	    	} catch(PDOException $e) {
	      		echo $e->getMessage();
	    	}
	  	}
	
	  	public function guardarReceta($db) {
	    	try {
	      		$datos = array('userid_id' => $this->id,
	                    	   'nombre' => $this->nombre,
	                    	   'tipo' => $this->tipo,
	                    	   'elaboracion' => $this->elaboracion,
	                    	   'tiempo' => $this->tiempo,
	                    	   'imagen' => $this->imagen);
	
	      		$insert = "INSERT INTO aplicacion_recetas (userid_id, 
	      												nombre, 
	      												tipo, 
	      												elaboracion, 
	      												tiempo, 
	      												imagen) 
	      												
	                							VALUES (:userid_id, 
														:nombre, 
														:tipo, 
														:elaboracion, 
														:tiempo, 
														:imagen)";
	      		
	      		$stmt = $db->prepare($insert);
	      		$stmt->execute($datos);
	    	} catch (PDOException $e) {
	      		echo $e->getMessage();
	    	}
	  	}
	
	  	public static function borrar($db, $idUsuario, $idReceta) {
	  		
	    	// Buscamos el identificador de la receta
	    	try {
	      		$query = "SELECT * FROM aplicacion_recetas WHERE id = :id";
	      		$stmt = $db->prepare($query);
	      		$stmt->execute(array(':id' => $idReceta));
	      		$resultado = $stmt->fetch();
	    	} catch(PDOException $e) {
	      		echo $e->getMessage();
	    	}
	
	    	// Eliminamos la imagen asociada a la receta
	    	unlink("static/" . $resultado["imagen"]);
	
	    	// La receta de la tabla aplicacion_recetas es eliminada
	    	try {
	      		$delete = "DELETE FROM aplicacion_recetas WHERE id = :id";
	      		$stmt = $db->prepare($delete);
	      		$stmt->execute(array("id" => $idReceta));
	    	} catch (PDOException $e) {
	      		return false;
	    	}
	
	    	// También eliminamos las relaciones con los ingredientes de la tabla aplicacion_union
	    	try {
	      		$delete = "DELETE FROM aplicacion_union WHERE receta_id = :id";
	      		$stmt = $db->prepare($delete);
	      		$stmt->execute(array("id" => $idReceta));
	    	} catch (PDOException $e) {
	      		echo $e->getMessage();
	    	}
	
	    	return true;
	  	}
	
	  	public function editar($db) {
		    // Si la imagen ha sido actualizada
		    if ($this->imagen == '') {
		      	$datos = array('nombre' => $this->nombre,
		                       'tipo' => $this->tipo,
		                       'elaboracion' => $this->elaboracion,
		                       'tiempo' => $this->tiempo,
		                       'id' => $this->id);
		
		      	try {
			        $update = "UPDATE aplicacion_recetas SET nombre=:nombre, tipo=:tipo, elaboracion=:elaboracion, tiempo=:tiempo WHERE id=:id";
			        $stmt = $db->prepare($update);
			        $stmt->execute($datos);
		      	} catch (PDOException $e) {
		        	echo $e->getMessage();
		      	}
		    } else {
		    	$datos = array('nombre' => $this->nombre,
		                       'tipo' => $this->tipo,
		                       'elaboracion' => $this->elaboracion,
		                       'tiempo' => $this->tiempo,
		                       'imagen' => $this->imagen,
		                       'id' => $this->id);
		
		      	try {
		        	$update = "UPDATE aplicacion_recetas SET nombre=:nombre, 
		        										  tipo=:tipo, 
		        										  elaboracion=:elaboracion, 
		        										  tiempo=:tiempo, 
		        										  imagen=:imagen 
		        									
		        									WHERE id=:id";
		        									
		        	$stmt = $db->prepare($update);
		        	$stmt->execute($datos);
		      	} catch (PDOException $e) {
		        	echo $e->getMessage();
		      	}
		    }
		}
	}

?>