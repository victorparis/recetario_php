<?php 

	include 'basedatos.php';
	include 'vendor/twig.php';
	include 'usuarios.php';
	
	// Si el usuario ya esta logeado redirigimos
	if (isset($_SESSION["id"])) {
		header('location: index.php');
	  	exit;
	}
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$user = trim($_POST['user']);
	  	$pass = trim($_POST['pass']);
	
	  	// Registramos el usuario
	  	$usuario = new Usuario($user, $pass);
	  	if(!$usuario->guardar($db)) {
	    	$error = "Ya hay un usuario registrado con ese nombre";
	    	echo $twig->render('register.html', array("error" => $error));
	    	return;
	  	}
	
	  	// Lo logeamos
	  	$usuario->login($db);
	
	  	// Redirigimos al index
	  	header('location: index.php');
	  	exit;
	}
	
	// Renderizamos el template
	echo $twig->render('register.html');

?>