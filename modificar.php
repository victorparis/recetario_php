<?php

	include 'vendor/twig.php';
	include 'basedatos.php';
	include 'ingredientes.php';
	include 'recetas.php';

	// Función para obtener los datos de la receta y rellenar los campos del formulario
	function obtenerDatosReceta($db, $idReceta) {
	  // Obtenemos la receta
		try {
	 		$query = "SELECT * FROM aplicacion_recetas WHERE id = :id";
	    	$stmt = $db->prepare($query);
	    	$stmt->execute(array(':id' => $idReceta));
	    	$receta = $stmt->fetch();
	  	} catch(PDOException $e) {
	    	echo $e->getMessage();
	  	}
	
	  	// Generamos la lista de tipos con el tipo seleccionado por defecto
	  	$tipos = array("Entrante", "Primero", "Segundo", "Postre");
	  	$tiposArray = array();
	  	foreach ($tipos as $tipo) {
	    	if ($receta["tipo"] == $tipo) {
	      		$tiposArray[] = "<option value='" . $tipo . "' selected>" . $tipo . "</option>";
	      		continue;
	    	}
	
	   	 	$tiposArray[] = "<option value='" . $tipo . "'>" . $tipo . "</option>";
	  	}
	
	  	// Creamos el texto con los ingredientes separados por comas
	  	try {
	    	$query = "SELECT I.* FROM aplicacion_ingredientes AS I INNER JOIN aplicacion_union AS RI ON I.id = RI.ingrediente_id WHERE RI.receta_id = :id";
	    	$stmt = $db->prepare($query);
	    	$stmt->execute(array(':id' => $idReceta));
	    	$ingredientesArray = $stmt->fetchAll();
	  	} catch(PDOException $e) {
	    	echo $e->getMessage();
	  	}
	
	  	$ingredientes = "";
	
	  	foreach ($ingredientesArray as $ingrediente) {
	    	$ingredientes .= $ingrediente['nombre'] . ", ";
	 	}
	
	  	// Eliminamos la ultima coma del string
	  	$ingredientes = substr($ingredientes, 0, -2);
	
	  	return $recetaDatos = array("receta" => $receta, "tipos" => $tiposArray, "ingredientes" => $ingredientes);
	}
	
	// Si es POST
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$idReceta = $_POST['id'];
	
	  	// Procesamos el archivo subido
	  	// Obtenemos el tipo de fichero
	  	if (!empty($_FILES['img']['type'])) {
	    	$imgtipo = explode("/", $_FILES['img']['type'])[1];
	    	// Carpeta temporal del fichero
	    	$imgtmp = $_FILES['img']['tmp_name'];
	
	    	// Declaramos las extensiones admitidas
	    	$extensiones = array("jpeg", "jpg", "png");
	
	    	// Comprobamos que es una de ellas
	    	if (!in_array($imgtipo, $extensiones)) {
	      		$error = "Tipo de archivo no válido, por favor elige un archivo JPEG o PNG.";
	      		echo $twig->render('modificar.html', array("datos" => obtenerDatosReceta($db, $idReceta), "error" => $error));
	      		return;
	    	}
	
	    	// Generamos un nombre aleatorio
	    	$imgnombre = sha1(time());
	
	    	// Creamos la carpeta si no existe
	    	if(!is_dir("static/images/platos")) mkdir("static/images/platos");
	
	    	// Movemos la imagen a nuestra carpeta
	    	$destinoimg = "images/platos/" . $imgnombre . "." . $imgtipo;
	    	move_uploaded_file($imgtmp, "static/" . $destinoimg);
	
	    	// Guardamos la receta
	    	$receta = new Receta($idReceta, $_POST, $destinoimg);
	    	$receta->editar($db);
	  	} else {
	    	// Guardamos la receta
	    	$receta = new Receta($idReceta, $_POST, "");
	    	$receta->editar($db);
	  	}
	
	  	// Procesamos los ingredientes
	  	/* Borramos todas las relaciones de la tabla receta_ingredientes
	 	para grabar las nuevas, asi nos evitamos tener que comprobar que ingredientes
	  	ha quitado y cuales ha puesto nuevos */
	  	Receta::borrarIngrediente($db, $_POST['id']);
	
	  	// Eliminamos espacios al principio y final del string
	  	$ingredientes = trim($_POST['ing']);
	  	// Creamos un array separando las palabras por las comas
	  	$ingredientes = explode(",", $ingredientes);
	
	  	// Los guardamos en la bbdd
	  	foreach ($ingredientes as $ingrediente) {
	    	$ingrediente = trim($ingrediente);
	
	    	// Comprobamos que no sean solo espacios
	    	if (!ctype_space($ingrediente)) {
	      		$ing = new Ingrediente($ingrediente);
	      		$ing->guardarIngredientes($db);
	
	      		// Añadimos a la tabla aplicacion_union las relaciones
	      		// entre cada receta y sus ingredientes
	      		try {
	        		$insert = "INSERT INTO aplicacion_union (receta_id, 
	        															 ingrediente_id) 
	        													 
	        													 VALUES (:receta_id, 
	        													 		 :ingrediente_id)";
																		 
	        	$stmt = $db->prepare($insert);
	        	$stmt->execute(array("receta_id" => Receta::obtenerIdReceta($db, trim($_POST["nombre"])), 
	        						 "ingrediente_id" => Ingrediente::obtenerIdIngredientes($db, $ingrediente)));
	      		} catch (PDOException $e) {
	        		echo $e->getMessage();
	        		return;
	      		}
	    	}
	  	}
	
	  	// Redirigimos a la receta
	  	header('location: detalle.php?id=' . Receta::obtenerIdReceta($db, trim($_POST["nombre"])));
	  	exit;
	}
	
	// Si es GET
	
	// Si el usuario no esta logeado redirigimos
	if (!isset($_SESSION["id"])) {
	  	header('location: index.php');
	  	exit;
	}
	
	
	// Buscamos la receta
	$idReceta = $_GET['id'];
	try {
	  	$query = "SELECT * FROM aplicacion_recetas WHERE id = :id";
	  	$stmt = $db->prepare($query);
	  	$stmt->execute(array(':id' => $idReceta));
	  	$receta = $stmt->fetch();
	} catch(PDOException $e) {
	  	echo $e->getMessage();
	}
	
	
	// Aplicamos los cambios en la vista de modificar receta
	echo $twig->render('modificar.html', array("datos" => obtenerDatosReceta($db, $idReceta)));

?>