<?php

	include 'vendor/twig.php';
	include 'basedatos.php';
	include 'ingredientes.php';
	include 'recetas.php';

	if (!isset($_SESSION["id"])) {
	  	header('location: index.php');
	  	exit;
	}
	
	// Si se llama al archivo por POST
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
	  	$imgtipo = explode("/", $_FILES['img']['type'])[1];
	  	$imgtmp = $_FILES['img']['tmp_name'];
	  	$extensiones = array("jpeg", "jpg", "png");
	  	if (!in_array($imgtipo, $extensiones)) {
	      	$error = "Extensión de imagen no permitida.";
	    	echo $twig->render('nueva_receta.html', array("error" => $error));
	    	return;
	  	}
	
	  	$imgnombre = sha1(time());
	
	  	if(!is_dir("static/images/recetas")) mkdir("static/images/recetas");
	
	  	$destinoimg = "images/recetas/" . $imgnombre . "." . $imgtipo;
	  	move_uploaded_file($imgtmp, "static/" . $destinoimg);
	
	  	$receta = new Receta($_SESSION['id'], $_POST, $destinoimg);
	  	$receta->guardarReceta($db);
	
	  	$ingredientes = trim($_POST['ing']);
	    $ingredientes = explode(",", $ingredientes);
	
	  	foreach ($ingredientes as $ingrediente) {
	    	$ingrediente = trim($ingrediente);
	    	if (!ctype_space($ingrediente)) {
	      		$ing = new Ingrediente($ingrediente);
	      		$ing->guardarIngredientes($db);
	      		try {
	        		$insert = "INSERT INTO aplicacion_union (receta_id, 
	        						 									 ingrediente_id) 
	        						 							VALUES (:receta_id, 
	        						 									:ingrediente_id)";
	        
	        	$stmt = $db->prepare($insert);
	        	$stmt->execute(array("receta_id" => Receta::getId($db, trim($_POST["nombre"])), 
	        	 					 "ingrediente_id" => Ingrediente::getId($db, $ingrediente)));
	      		} catch (PDOException $e) {
	        		echo $e->getMessage();
	        		return;
	      		}
	    	}
	  	}
	
	  	// Redirigimos a la receta
	  	header('location: detalle.php?id=' . Receta::getId($db, trim($_POST["nombre"])));
	  	exit;
	}
	
	// Aplicamos los cambios en la vista de la nueva receta:
	echo $twig->render('nueva_receta.html');

?>