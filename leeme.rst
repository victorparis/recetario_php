Instalación y configuración de Recetario PHP
===============================================

* Instalar composer en el proyecto:
	curl -s http://getcomposer.org/installer | php
* Crea un composer.json en la raíz del proyecto:
	{
		"require": {
			"twig/twig": "1.*"
		}
	}
* Instalar:
	php composer.phar install
* Le damos permisos de escritura a la carpeta donde va a estar el proyecto:
	sudo chmod -R 777 nombre_del_proyecto/
* Hay que modificar **<base href="/ejerciciosphp/recetarioPHP/">** del archivo "vistas/base.html" por la ruta correcta.
* Ya podemos ejecutar la aplicación.


Funcionalidad
================

* Esta aplicación permite la creación, modificación y eliminación de recetas mediante PHP.
* Un usuario podrá ver toda la lista de recetas almacenadas en la base de datos, pero no podrá modificar más que las propias.



