<?php

	require_once 'vendor/autoload.php';

	$loader = new Twig_Loader_Filesystem('vistas');
	$twig = new Twig_Environment($loader);

	session_start();
	$twig->addGlobal("session", $_SESSION);

?>