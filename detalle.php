<?php

	include 'basedatos.php';
	include 'vendor/twig.php';
	
	// Se localiza la receta que corresponde al identificador
	$id = $_GET['id'];
	try {
		$query = "SELECT * FROM aplicacion_recetas WHERE id = :id";
	  	$stmt = $db->prepare($query);
	  	$stmt->execute(array(':id' => $id));
	  	$receta = $stmt->fetch();
	} catch(PDOException $e) {
	  	echo $e->getMessage();
	}
	
	
	// Se asocia a la receta sus respectivos ingredientes
	try {
	  	$query = "SELECT * FROM aplicacion_ingredientes AS I JOIN aplicacion_union AS RI ON RI.receta_id = :id AND RI.ingrediente_id = I.id";
	  	$stmt = $db->prepare($query);
	  	$stmt->execute(array(':id' => $id));
	  	$ingredientes = $stmt->fetchAll();
	} catch(PDOException $e) {
	  	echo $e->getMessage();
	}
	
	$db = null;
	
	// Aplicamos los cambios en la vista de detalle
	echo $twig->render('detalle.html', array('receta' => $receta, 'ingredientes' => $ingredientes));

?>